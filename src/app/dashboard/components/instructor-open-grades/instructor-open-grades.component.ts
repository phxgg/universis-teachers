import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {CoursesService} from '../../../courses/services/courses.service';
import {ErrorService} from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-instructor-open-grades',
  templateUrl: './instructor-open-grades.component.html',
  styleUrls: ['./instructor-open-grades.component.scss']
})
export class InstructorOpenGradesComponent implements OnInit {

  public openCourseExams: any = []; // Data
  public isLoading = true;          // Only if data is loaded


  constructor(private _context: AngularDataContext,
              private _coursesService: CoursesService,
              private _errorService: ErrorService,
              private _profileService: ProfileService) { }

  async ngOnInit() {
    let instructor: any;
    let courseExams: any[];
    let currentYear: number;

    try {
      this.isLoading = true;
      instructor = await this._profileService.getInstructor();
      currentYear = 0;
      if (instructor && instructor.department && instructor.department.currentYear) {
        currentYear = instructor.department.currentYear.id;
      }

      const previousYear = currentYear - 1;
      courseExams = await this._coursesService.getOpenExamsSinceYear(previousYear);

      this.openCourseExams = courseExams.map((x) => {
        if (x.classes && x.classes.length > 0) {
          x.classes.sort((a, b) => {
            return a.courseClass.period.id > b.courseClass.period.id ? -1 : 1;
          });
          // set courseExam url using latest courseClass
          x.url = `/courses/${x.course.id}/${x.classes[0].courseClass.year.id}/${x.classes[0].courseClass.period.id}/exams/${x.id}`;
        }
        return x;
      }).filter(courseExam => courseExam.classes && courseExam.classes.length > 0);
    } catch (err) {
      console.error(err);
      return this._errorService.navigateToError(err);
    } finally {
      this.isLoading = false;
    }
  }

}
