import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {DiagnosticsService} from '@universis/common';

@Injectable({
  providedIn: 'root'
})
export class AvailableServiceGuard implements CanActivate {
  constructor(
    private _diagnosticService: DiagnosticsService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const dependencies = next.data.serviceDependencies;
    if (!(dependencies && dependencies.length)) {
      return true;
    }

    const hasServices = dependencies.map(d => this._diagnosticService.hasService(d));
    return Promise.all(hasServices).then(result => {
      return !result.includes(false);
    }).catch(err => {
      console.log(err);
      return false;
    });
  }
}
