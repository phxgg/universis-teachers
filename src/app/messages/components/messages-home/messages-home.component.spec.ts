import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import { MessagesHomeComponent } from './messages-home.component';

describe('MessagesHomeComponent', () => {
  let component: MessagesHomeComponent;
  let fixture: ComponentFixture<MessagesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ MessagesHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    const messagesHomeComponent = new MessagesHomeComponent();
    expect(messagesHomeComponent).toBeTruthy();
  });
});
