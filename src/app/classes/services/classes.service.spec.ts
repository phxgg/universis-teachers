import { TestBed } from '@angular/core/testing';

import { ClassesService } from './classes.service';
import { MostModule } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClassesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      }),
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: ClassesService = TestBed.get(ClassesService);
    expect(service).toBeTruthy();
  });
});
