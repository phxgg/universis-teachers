import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class ConsultedStudentsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./consulted-students-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./consulted-students-table.config.list.json`);
        });
    }
}

export class ConsultedStudentsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./consulted-students-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./consulted-students-table.search.list.json`);
            });
    }
}

export class ConsultedStudentsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./consulted-students-table.config.list.json`);
    }
}
