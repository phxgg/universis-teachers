import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsultedStudentsTableComponent } from './consulted-students-table.component';



describe('ConsultedStudentsTableComponent', () => {
  let component: ConsultedStudentsTableComponent;
  let fixture: ComponentFixture<ConsultedStudentsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultedStudentsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultedStudentsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
