import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesCompletedComponent } from './theses-completed.component';
import { NgPipesModule } from 'ngx-pipes';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalModule } from 'ngx-bootstrap';
import { ErrorModule } from '@universis/common';

describe('ThesesCompletedComponent', () => {
  let component: ThesesCompletedComponent;
  let fixture: ComponentFixture<ThesesCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgPipesModule,
        RouterTestingModule,
        SharedModule.forRoot(),
        TranslateModule.forRoot(),
        ModalModule.forRoot(),
        ErrorModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [ ThesesCompletedComponent ],
      providers: [
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
