import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LogoutComponent} from '@universis/common';
import {ErrorBaseComponent, HttpErrorComponent} from '@universis/common';
import {AuthGuard} from '@universis/common';
import {LangComponent} from './teachers-shared/lang-switch.component';
import {ProfileModule} from './profile/profile.module';
import { InstructorOutlineTabsComponent } from '@universis/ngx-qa';
import { InstructorOutlineGeneralComponent } from '@universis/ngx-qa';
import { InstructorOutlinePublicationsComponent } from '@universis/ngx-qa';
import { AvailableServiceGuard } from './available-service.guard';

export const routes: Routes = [
  {
    path: 'lang/:id/:route',
    component: LangComponent
  },
  {
    path: 'lang/:id/:route/:subroute',
    component: LangComponent,
    pathMatch: 'prefix'
  },
  {
    path: 'error/498',
    redirectTo: 'auth/loginAs'
  },
  {
    path: 'error/401.1',
    redirectTo: 'auth/loginAs'
  },
  {
    path: 'error/0',
    redirectTo: 'error/408.1'
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule'
      },
      {
        path: 'theses',
        loadChildren: './theses/theses.module#ThesesModule'
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      },
      {
        path: 'info',
        loadChildren: './info-pages/info-pages.module#InfoPagesModule'
      },
      {
        path: 'messages',
        loadChildren: './messages/messages.module#MessagesModule'
      },
      {
        path: 'outline',
        component: InstructorOutlineTabsComponent,
        canActivate: [
          AvailableServiceGuard
        ],
        data : {
          serviceDependencies: [
            'QualityAssuranceService'
          ]
        },
        children: [
          {
            path: '',
            redirectTo: 'general',
            pathMatch: 'full'
          },
          {
            path: 'general',
            component: InstructorOutlineGeneralComponent
          },
          {
            path: 'publications',
            component: InstructorOutlinePublicationsComponent
          }
        ]
      },
      {
        path: 'consultedStudents',
        loadChildren: './consulted-students/consulted-students.module#ConsultedStudentsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
